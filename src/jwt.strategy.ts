import { Injectable } from '@nestjs/common';
import { ContextIdFactory, ModuleRef } from '@nestjs/core';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { config } from "./config";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private moduleRef: ModuleRef) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'TestSecretKey',
      passReqToCallback: true,
    });
  }

  async validate(request: Request, reqPayload: any): Promise<any> {
    const contextId = ContextIdFactory.getByRequest(request);
    console.log(contextId);

  }
}
