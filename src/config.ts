import 'dotenv-safe/config';

export interface Config {
    JWT_SECRET_KEY: string;
}


export const config: Config = Object.freeze({
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
});